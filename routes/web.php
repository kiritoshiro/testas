<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware' => ['auth']], function() {

    Route::get('/', 'PrekeController@index')->name('home');
    Route::get('/delete/{id}', 'PrekeController@destroy')->name('delete');
    Route::get('/create', function(){
        return view('create');
        })->name('open.create');
    Route::post('/create', 'PrekeController@create')->name('create');
    Route::get('/update/{id}', 'PrekeController@openUpdate')->name('open.update');

    Route::patch('/update/{id}', 'PrekeController@update')->name('update');
    Route::get('/filter', 'PrekeController@filter')->name('filter');
});

