<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preke extends Model
{
    const CATEGORY = 'category'; //kategorija
    const MODEL ='model';        //modelis
    const MAKE = 'make';         //gamintojas
    const STOCK = 'stock';       //sandėlyja


    protected $fillable = [
        self::CATEGORY,
        self::MODEL,
        self::MAKE,
        self::STOCK,
    ];

    public function scopeFilters($query, $filters){
        if (isset($filters['category'])){
            $query->where('category', 'LIKE', '%'. $filters['category'].'%');
        }
        if (isset($filters['model'])){
            $query->where('model', 'LIKE', '%'. $filters['model'].'%');
        }
        if (isset($filters['make'])){
            $query->where('make', 'LIKE', '%'. $filters['make'].'%');
        }
        if (isset($filters['stock'])){
            $query->where('stock', '=', $filters['stock']);
        }
    }


    public function getCategory(){
        return $this->getAttribute(self::CATEGORY);
    }
    public function setCategory($val){
        $this->setAttribute(self::CATEGORY, $val);
    }

    public function getModel(){
        return $this->getAttribute(self::MODEL);
    }
    public function setModel($val){
        $this->setAttribute(self::MODEL, $val);
    }

    public function getMake(){
        return $this->getAttribute(self::MAKE);
    }
    public function setMake($val){
        $this->setAttribute(self::MAKE, $val);
    }

    public function getStock(){
        return $this->getAttribute(self::STOCK);
    }
    public function setStock($val){
        $this->setAttribute(self::STOCK, $val);
    }
}
