<?php

namespace App\Http\Controllers;

use App\Preke;
use Illuminate\Http\Request;

class PrekeController extends Controller
{

    public function index()
    {
        $preke = Preke::paginate(20);
        return view ('home', [
            'prekes' => $preke
        ]);
    }

    public function create(Request $request)
    {
        $preke = Preke::create($request->all());
        return redirect()->route('home');
    }

    public function openUpdate($id){
        $preke = Preke::findOrFail($id);
        return view('update',[
           'preke' => $preke
        ]);
    }
    public function update(Request $request, $id)
    {
        $preke = Preke::findOrFail($id);
        $preke->category = $request->category;
        $preke->model = $request->model;
        $preke->make = $request->make;
        $preke->stock = $request->stock;
        $preke->save();
        return redirect()->route('home');
    }

    public function destroy(Request $request, $id)
    {
        $preke = Preke::findOrFail($id);
        $preke->delete();
        return redirect()->back();
    }

    public function filter(Request $request){
        $entries = Preke::filters($request->all())->paginate(20);
        return view ('home', [
            'prekes' => $entries
        ]);
    }
}
