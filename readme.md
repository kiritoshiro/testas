# Testinis darbas

### Steps to setup the project
1. ```git clone https://kiritoshiro@bitbucket.org/kiritoshiro/testas.git```
2. ```cd testas``` and ```cp .env.example .env```
3. Open .env file and setup database name with MySQL login credentials
4. ```npm install```
5. ```composer install```
6. ```php artisan key:generate```
7. ```php artisan migrate```
8. ```php artisan db:seed```

### To run project
1. ```php artisan serve```
2. ```npm run dev```
3. ```http://127.0.0.1:8000/```
