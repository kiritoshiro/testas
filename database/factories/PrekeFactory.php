<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Preke::class, function (Faker $faker) {

    $category = [
        'Fotoaparatai',
        'Kompiuteriai',
        'Telefonai',
        'šaldytuvai',
        'Printeriai'
    ];

    $model = [
        'Canon sf 123',
        'Sony ss 123',
        'Asus 123',
        'HP123',
        'Samsung GSII',
        'Iphone 5'
    ];

    $make = [
        'Canon',
        'Sony',
        'Asus',
        'HP',
        'Samsung',
        'Apple'
    ];

    return [
        'category' => $category[rand(0, count($category)-1)],
        'model' => $model[rand(0, count($model)-1)],
        'make' => $make[rand(0, count($make)-1)],
        'stock' => $faker->numberBetween(0,1)
    ];
});
