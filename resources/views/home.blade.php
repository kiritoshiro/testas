@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Prekių sarašas</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" id="form_create" action="{{ route('create')}}"></form>
                        <form method="GET" id="filter" action="{{ route('filter') }}"></form>

                        <a href="{{ route('open.create') }}">+Sukurti naują</a>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th><strong>ID</strong></th>
                                <th><strong>Kategorija</strong></th>
                                <th><strong>Modelis</strong></th>
                                <th><strong>Gamintojas</strong></th>
                                <th><strong>Sandėlyje</strong></th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                {{-- FILTER --}}
                                <td></td>
                                <td><input id="txt_category" type="text" form="filter" class="form-control"
                                           name="category" value="{{ app('request')->input('category') }}"/></td>
                                <td><input id="txt_model" type="text" form="filter" class="form-control" name="model"
                                           value="{{ app('request')->input('model') }}"/></td>
                                <td><input id='txt_make' type="text" form="filter" class="form-control" name="make"
                                           value="{{ app('request')->input('make') }}"/></td>
                                <td><select id="txt_stock" form="filter" class="form-control" name="stock">

                                        <option @if (app('request')->input('stock') == 1) {{ 'selected' }} @endif value="1">Taip</option>
                                        <option @if (app('request')->input('stock') == 0) {{ 'selected' }} @endif value="0">Ne</option>
                                        <option @if (app('request')->input('stock') == '') {{ 'selected' }} @endif value="">Taip/Ne</option>
                                    </select>
                                </td>

                                <td>
                                    <button type="submit" form="filter" class="btn btn-primary">Ieškoti</button>
                                </td>
                            </tr>

                            @foreach($prekes as $preke)
                                <tr>
                                    <td>{{ $preke->id }}</td>
                                    <td>{{ $preke->category }}</td>
                                    <td>{{ $preke->model }}</td>
                                    <td>{{ $preke->make }}</td>
                                    <td>
                                        @if ( $preke->stock  == 1) Taip
                                        @else Ne
                                        @endif
                                    </td>

                                    <td>
                                        <a href="{{ route('open.update', $preke->id)  }}">Atnaujinti </a>|
                                        <a href="{{ route('delete', $preke->id) }}">Ištrinti</a>
                                    </td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {{ $prekes->appends(request()->query())->links() }} {{--Pagination links--}}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
