@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Sukurti naują įrašą</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="card-body">

                            <form method="POST" id="create" action="{{ route('create')}}">
                                @csrf
                                <div class="form-group row">
                                    <label for="category"
                                           class="col-md-4 col-form-label text-md-right">Kategorija</label>

                                    <div class="col-md-6">
                                        <input id="category" type="text" form="create"
                                               class="form-control @error('category') is-invalid @enderror"
                                               name="category" autofocus>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="model" class="col-md-4 col-form-label text-md-right">Modelis</label>

                                    <div class="col-md-6">
                                        <input id="model" type="text" form="create"
                                               class="form-control @error('model') is-invalid @enderror" name="model">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="make" class="col-md-4 col-form-label text-md-right">Gamintojas</label>

                                    <div class="col-md-6">
                                        <input id="make" type="text" form="create"
                                               class="form-control @error('make') is-invalid @enderror" name="make">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="stock" class="col-md-4 col-form-label text-md-right">Sandėlyje</label>

                                    <div class="col-md-2">
                                        <select id="stock" class="form-control"  form="create" name="stock">
                                            <option selected value="1">Taip</option>
                                            <option value="0">Ne</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-5">
                                        <button type="submit" class="btn btn-primary" form="create">
                                            Sukurti
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
