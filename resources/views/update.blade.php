@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Atnaujinti įrašo duomenis</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="card-body">

                            <form method="POST" id="update" action="{{ route('update', [$preke])}}">
                                @method('patch')
                                @csrf
                                <div class="form-group row">
                                    <label for="category"
                                           class="col-md-4 col-form-label text-md-right">Kategorija</label>

                                    <div class="col-md-6">
                                        <input id="category" type="text" form="update" value="{{ $preke->category }}"
                                               class="form-control" name="category" autofocus>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="model" class="col-md-4 col-form-label text-md-right">Modelis</label>

                                    <div class="col-md-6">
                                        <input id="model" type="text" form="update" value="{{ $preke->model }}"
                                               class="form-control" name="model">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="make" class="col-md-4 col-form-label text-md-right">Gamintojas</label>

                                    <div class="col-md-6">
                                        <input id="make" type="text" form="update" value="{{ $preke->make }}"
                                               class="form-control" name="make">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="stock" class="col-md-4 col-form-label text-md-right">Sandėlyje</label>

                                    <div class="col-md-2">
                                        <select id="stock" class="form-control"  form="update" name="stock">
                                            <option @if ( ($preke->stock) == 1) {{ 'selected' }} @endif value="1">Taip</option>
                                            <option @if ( ($preke->stock) == 0) {{ 'selected' }} @endif value="0">Ne</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-5">
                                        <button type="submit" class="btn btn-primary" form="update">
                                            Atnaujinti
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
